;;; towel.el --- Tags Oriented Welfare Ensurance Lists  -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Mihail Iosilevich

;; Author: Mihail Iosilevich <yosik@paranoid.email>
;; Keywords: calendar, convenience
;; Version: 0.1
;; Package-Requires: ((emacs "28.1"))
;; Homepage: https://codeberg.org/yosik/towel

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; My attempt to make tag-based TODO list management tool.

;; * General Idea
;; Each task is just a list of tags like '(tag1 tag2 tag3), nothing more
;;
;; Tags can have subtags like '(tag1 (tag2 tag3) tag4)
;;
;; ** Representing useful data
;; Tags are not forced to be symbols, so we can represent task as a
;; simple "list of tags":
;; '((priority . 10) (name . "Frobnicate smth") someday)
;;
;; ** ID
;; For ease of implementation each task must have an ID field:
;; '(tag1 (name . "Do work") (id . <some-value>))
;;
;; I am not sure if it should be something human-readable (Lisp symbol?
;; Use `gensym' for lazy users?) or (sha1 (format "%f" (float-time))).
;; Probably numbers are nice: they are not too hard to type and lazy
;; users will be happy. Anyway, ID can be represented in any format,
;; just customize `towel-tasks-equal-p', `towel-get-tags-function' and
;; `towel-gen-new-id-function'. In theory user can get rid of ID
;; completely but it's hard to imagine reason for this.
;;
;; * Customizability
;; Users can write their own predicates for filtering and sorting. See
;; `towel-filter' and `towel-sort' variables.
;;
;; ** Examples
;; See towel-my.el (it is a config which I use daily). Its features:
;; - Tasks can be scheduled on certain time (and can repeat every second
;;   saturday). Can your org-mode do that?
;; - Tasks can be sorted by priority and complexity
;; - Tasks can be repeated (for example, my task "Clean teeth" repeats
;;   twice a day)
;; - Certain tags are highlighted peculiary (for example, `project')
;; - DWIM way of markings tasks as done

;;; Code:

(require 'ewoc)
(require 'seq)
(require 'pp)
(require 'minibuffer)                   ; For `format-prompt'
(require 'lisp-mode)                    ; For `lisp-data-mode'
(eval-when-compile
  (require 'subr-x))

(defgroup towel nil
  "Tag-based TODO list management tool."
  :prefix "towel-"
  :group 'applications)

;;;; Faces

(defface towel-header
  '((t :inherit font-lock-type-face))
  "Face for headers in towel buffers."
  :group 'towel)

(defface towel-id
  '((t :inherit font-lock-constant-face))
  "Face for ID in towel buffers."
  :group 'towel)

(defface towel-tags
  '((t :inherit font-lock-keyword-face))
  "Face for tags in towel buffers."
  :group 'towel)

;;;; Custom variables

(defcustom towel-file (locate-user-emacs-file "towel-tasks")
  "File in which to save tasks by default."
  :group 'towel
  :type 'file)

(defcustom towel-sort (quote #'ignore)
  "Predicate for sorting tasks.

`towel-preprocess-sort-function' is used to transform value of
this variable into function which takes two arguments - both are tasks."
  :group 'towel
  :type 'sexp)

(defcustom towel-filter (quote #'always)
  "Predicate for filtering tasks.

`towel-preprocess-filter-function' is used to transform value of
this variable into function which takes one argument - a task.

For example, when value of `towel-preprocess-filter-function' is
`eval', this predicate will leave only tasks without `done' tag:

    (quote
     (lambda (task)
       (not (seq-contains-p task \\='done))))"
  :group 'towel
  :type 'sexp)

(defcustom towel-preprocess-sort-function #'eval
  "Function to preprocess value of `towel-sort'.

\(funcall towel-preprocess-sort-function towel-sort) should
return function of two arguments. This function will be used as
comparator when sorting tasks."
  :group 'towel
  :type 'function)

(defcustom towel-preprocess-filter-function #'eval
  "Function to preprocess value of `towel-filter'.

\(funcall towel-preprocess-filter-function towel-filter) should
return function of one arguments. This function will be used to
filter tasks."
  :group 'towel
  :type 'function)

(defcustom towel-tasks-equal-p
  (lambda (task-1 task-2)
    (let ((id-1 (alist-get 'id task-1))
          (id-2 (alist-get 'id task-2)))
      (and id-1 id-2 (= id-1 id-2))))
  "Predicate for comparing two tasks."
  :group 'towel
  :type 'function)

(defcustom towel-gen-new-id-function
  (lambda ()
    "Return new id which does not collide with existing ones."
    (1+ (apply #'max
               (mapcar (lambda (task) (alist-get 'id task))
                       (towel--read)))))
  "Function to generate an id for new task."
  :group 'towel
  :type 'function)

(defcustom towel-get-tags-function
  (lambda (task)
    "Get TASK tags without metadata (for example, `id' or `name')."
    (seq-remove
     (lambda (tag)
       (and (listp tag)
            (seq-contains-p '(id name)
                            (car tag))))
     task))
  "Function to get tags from task without metadata (like id or name)."
  :group 'towel
  :type 'function)

(defcustom towel-pp-task-function
  (lambda (task)
    "Pretty-print TASK."
    (insert (format "%3s - %s - (%s)"
                    (propertize
                     (number-to-string (alist-get 'id task))
                     'face 'towel-id)
                    (alist-get 'name task)
                    (string-join
                     (mapcar
                      (lambda (tag)
                        (propertize (format "%S" tag)
                                    'face 'towel-tags))
                      (funcall towel-get-tags-function task))
                     " "))))
  "Function to pretty-print task."
  :group 'towel
  :type 'function)

;;;; Internal functions and variables

(defvar-local towel--ewoc nil
  "EWOC object for `towel-mode' buffers.")

(defun towel--insert-pp (thing)
  "Insert pretty-printed THING into current buffer.

All abbreviations are disabled."
  (let ((print-level nil)
        (print-length nil))
    (pp thing (current-buffer))))

(defun towel--read ()
  "Read towel tasks from `towel-file'."
  (when (file-exists-p towel-file)
    (with-temp-buffer
      (insert-file-contents towel-file)
      (read (buffer-string)))))

(defcustom towel-before-write-hook nil
  "Hook run before tasks are written into `towel-file'.

Can be used to do backups and versioning."
  :group 'towel
  :type 'hook)

(defun towel--write (tasks)
  "Write TASKS into `towel-file'."
  (make-directory (file-name-directory towel-file) t)
  (run-hooks 'towel-before-write-hook)
  (with-temp-file towel-file
    (insert ";;; -*- lisp-data -*-\n")
    (towel--insert-pp tasks)))

(defun towel--add (new-task)
  "Add NEW-TASK into `towel-file'."
  (towel--write
   (cons new-task
         (seq-remove
          (lambda (task)
            (funcall towel-tasks-equal-p task new-task))
          (towel--read)))))

(defun towel--pp-thing (thing)
  "Pretty-print THING.

Used as pretty-printer for `ewoc-create'. Strings are printed via
`pp', lists are printed via `towel-pp-task-function'."
  (cond
   ((stringp thing) (insert thing))
   ((listp thing) (funcall towel-pp-task-function thing))
   (t (format "Unknown object: %S" thing))))

(defun towel--task-under-point ()
  "Get task under point. Return nil if there is no such.."
  (let* ((node (ewoc-locate towel--ewoc))
         (data (and node (ewoc-data node))))
    (and (listp data) data)))

;;;; Commands and user-facing functions

(defun towel-add-tag (task tag)
  "Add TAG to TASK and store TASK in `towel-file'."
  (interactive
   (list (towel--task-under-point)
         (read-minibuffer "Enter tag: ")))
  (when task
    (towel--add
     (seq-uniq (cons tag task)))
    (when (called-interactively-p 'interactive)
      (towel-revert-buffer))))

(defun towel-del-tag (task tag)
  "Remove TAG from TASK and store TASK in `towel-file'."
  (interactive
   (let* ((task (towel--task-under-point))
          (tag (read
                (completing-read
                 (format-prompt "Enter tag" nil)
                 (mapcar (lambda (s)
                           (format "%S" s))
                         (funcall towel-get-tags-function task))))))
     (list task tag)))
  (when task
    (towel--add
     (remove tag task))
    (when (called-interactively-p 'interactive)
      (towel-revert-buffer))))

;;;;; Editing tasks in separate buffer

(defun towel-task-exit ()
  "Save task in current buffer in `towel-file'."
  (interactive)
  (towel--add
   (read (buffer-string)))
  (kill-buffer-and-window))

(defvar towel-task-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-x C-s") #'towel-task-exit)
    (define-key map (kbd "C-c C-c") #'towel-task-exit)
    (define-key map (kbd "C-c C-k") #'kill-buffer-and-window)
    map)
  "Keymap for `towel-task-mode'.")

(define-minor-mode towel-task-mode
  "Minor mode for editing task in separate buffers.

\\{towel-task-mode-map}"
  :lighter " Task"
  (setq-local
   header-line-format
   (substitute-command-keys
    "Edit, then exit with `\\[towel-task-exit]' or abort with \
`\\[kill-buffer-and-window]'")))

(defun towel--new-task-buffer (name)
  "Open new task buffer with NAME."
  (pop-to-buffer
   (get-buffer-create name))            ; Make new buffer
  (lisp-data-mode)                      ; Enable `lisp-data-mode'
  (towel-task-mode)                     ; Enable `towel-task-mode'
  (insert                               ; To make a new task:
   (format ";; New id: %d\n\n"
           (funcall towel-gen-new-id-function))))

(defun towel-edit-task ()
  "Edit task under point in separate buffer."
  (interactive)
  (let ((task (towel--task-under-point)))
    (when task
      (towel--new-task-buffer
       (format "*task %d*" (alist-get 'id task))) ; Make new buffer
      (towel--insert-pp task))))                  ; Insert task

(defun towel-add-task ()
  "Open empty task buffer to type new task."
  (interactive)
  (towel--new-task-buffer "*new task*"))

;;;;; Functions for *towel* buffer

(defun towel-next-task (arg)
  "Move cursor to the beginning of next task.

With prefix argument ARG, move that may lines."
  (interactive "p")
  (ewoc-goto-next towel--ewoc arg))

(defun towel-prev-task (arg)
  "Move cursor to the beginning of previous task.

With prefix argument ARG, move that may lines."
  (interactive "p")
  (ewoc-goto-prev towel--ewoc arg))

(defvar towel-filter-predicate-history nil
  "History for `towel-set-filter-predicate' function.")

(defun towel-set-filter-predicate (predicate)
  "Set filter predicate to PREDICATE in current buffer."
  (interactive
   (list (let ((current (format "%S" towel-filter)))
           (read-from-minibuffer (format-prompt "Filter predicate"
                                                current)
                                 nil minibuffer-local-map
                                 t 'towel-filter-predicate-history
                                 current))))
  (setq-local towel-filter predicate)
  (towel-revert-buffer))

(defvar towel-sort-predicate-history nil
  "History for `towel-set-sort-predicate' function.")

(defun towel-set-sort-predicate (predicate)
  "Set sort predicate to PREDICATE in current buffer."
  (interactive
   (list (let ((current (format "%S" towel-sort)))
           (read-from-minibuffer (format-prompt "Sort predicate"
                                                current)
                                 nil minibuffer-local-map
                                 t 'towel-sort-predicate-history
                                 current))))
  (setq-local towel-sort predicate)
  (towel-revert-buffer))

(defun towel-get-matching-tasks (filter comparator)
  "Get tasks that match FILTER. Results are sorted with COMPARATOR."
  (seq-sort
   (funcall towel-preprocess-sort-function comparator)
   (seq-filter
    (funcall towel-preprocess-filter-function filter)
    (towel--read))))

(defun towel-revert-buffer (&optional _ignore-auto _noconfirm)
  "Re-read tasks, re-apply filter, re-sort and re-display."
  (interactive)
  (let ((old-point (point)))            ; Save current point
    ;; Remove all nodes:
    (ewoc-filter towel--ewoc #'ignore)
    ;; Insert headers:
    (ewoc-enter-last towel--ewoc
                     (format "%s %S\n"
                             (propertize "Filter:" 'face 'towel-header)
                             towel-filter))
    (ewoc-enter-last towel--ewoc
                     (format "%s %S\n"
                             (propertize "Sort:" 'face 'towel-header)
                             towel-sort))
    ;; Insert tasks:
    (mapc (lambda (task) (ewoc-enter-last towel--ewoc task))
          ;; filtered with `towel-filter' and sorted with `towel-sort'
          (towel-get-matching-tasks towel-filter towel-sort))
    ;; Return to previously saved position, otherwise point will jump to
    ;; the beginning of buffer
    (goto-char old-point)))

(defvar towel-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "n") #'towel-next-task)
    (define-key map (kbd "p") #'towel-prev-task)
    (define-key map (kbd "f") #'towel-set-filter-predicate)
    (define-key map (kbd "s") #'towel-set-sort-predicate)
    (define-key map (kbd "+") #'towel-add-tag)
    (define-key map (kbd "-") #'towel-del-tag)
    (define-key map (kbd "e") #'towel-edit-task)
    (define-key map (kbd "RET") #'towel-edit-task)
    (define-key map (kbd "a") #'towel-add-task)
    map)
  "Keymap for TOWEL buffer.")

(define-derived-mode towel-mode special-mode "TOWEL"
  "Major mode for browsing list of tasks.

\\{towel-mode-map}"
  :group 'towel
  (buffer-disable-undo)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (remove-overlays)
  (setq-local towel--ewoc (ewoc-create #'towel--pp-thing))
  (setq-local revert-buffer-function #'towel-revert-buffer)
  (towel-revert-buffer))

;;;###autoload
(defun towel-show ()
  "Show towel buffer with current tasks."
  (interactive)
  (pop-to-buffer (get-buffer-create "*towel*"))
  (towel-mode))

(provide 'towel)
;;; towel.el ends here

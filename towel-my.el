;;; towel-my.el --- Personal towel.el config         -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mihail Iosilevich

;; Author: Mihail Iosilevich <yosik@paranoid.email>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'towel)
(require 'seq)

(eval-when-compile
  (require 'subr-x))

(setq towel-file (locate-user-emacs-file "towel-tasks"))

(defun towel-my-task-urgency (task)
  "Return TASK urgency."
  (+ (float (expt (alist-get 'priority task) 1.1))
     (float (alist-get 'complexity task))))

(defcustom towel-my-day-start (* 3 60 60)
  "Number of seconds after midnight at which your day ends."
  :group 'towel-my
  :type 'integer)

(defun towel-my-round-seconds (timestamp)
  "Return TIMESTAMP without seconds and hours."
  (let ((d (decode-time (seconds-to-time timestamp))))
    (time-to-seconds
     (encode-time
      (list 0 0 0
            (decoded-time-day d)
            (decoded-time-month d)
            (decoded-time-year d)
            nil
            (decoded-time-dst d)
            (decoded-time-zone d))))))



(defvar towel-my-common-flet-funcs
  '((get (task property) (let ((val (alist-get property task)))
                           (if (and (consp val) (null (cdr val)))
                               (car val)
                             val)))
    (name (task) (get task 'name))
    (sched (task) (get task 'sched))
    (urg #'towel-my-task-urgency)
    (in #'seq-contains-p)
    (today (task)
           (<= (or (sched task) 0)
               (towel-my-round-seconds
                (- (time-to-seconds)
                   towel-my-day-start)))))
  "Functions to use in various `towel-...-preprocess-function'.")

(setq towel-preprocess-filter-function
      (lambda (body)
        (eval
         `(lambda (task)
            (cl-flet* (,@towel-my-common-flet-funcs
                       (+ (tag) (in task tag))
                       (- (tag) (not (in task tag))))
              ;; Can ur Scheme do that?
              (let ((name (name task))
                    (urg (urg task))
                    (today (today task))
                    (done (+ 'done))
                    (self (+ 'self))
                    (disabled (+ 'disabled)))
                ,body))))))

(setq towel-filter
      (quote
       (and (not (or done disabled)) today)))

(setq towel-preprocess-sort-function
      (lambda (body)
        (eval `(lambda (task-1 task-2)
                 (cl-flet* ,towel-my-common-flet-funcs
                   ,body)))))

(setq towel-sort
      (quote
       (< (urg task-1)
          (urg task-2))))



(setq towel-get-tags-function
      (lambda (task)
        "Get TASK tags without metadata (for example `id' or `name')."
        (seq-remove
         (lambda (tag)
           (and (listp tag)
                (seq-contains-p '(id
                                  priority complexity name
                                  sched next-time
                                  repetitions disabled)
                                (car tag))))
         task)))

(defface towel-my-urgency
  '((t :inherit warning))
  "Face used to print urgency in towel buffers."
  :group 'towel)

(defface towel-my-done
  '((t :strike-through t :inherit shadow))
  "Face used to show whether task has `done' status in towel buffers."
  :group 'towel)

(defface towel-my-date
  '((t :inherit success))
  "Face used to print scheduled date of task."
  :group 'towel)

(setq towel-pp-task-function
      (lambda (task)
        "Pretty-print TASK."
        (insert
         ;; 1 - priority
         ;; 2 - sched
         ;; 3 - repetitions
         ;; 4 - name
         ;; 6 - tags
         ;;          1  2 3 4   6
         (format " [%s]%s%s %s (%s)"
                 ;; priority
                 (propertize
                  (format "%.1f" (towel-my-task-urgency task))
                  'face 'towel-my-urgency)
                 ;; sched
                 (if-let ((sched (alist-get 'sched task)))
                     (propertize
                      (format-time-string
                       " %d.%m"
                       (seconds-to-time
                        sched))
                      'face 'towel-my-date)
                   "")
                 ;; repetitions
                 (if-let ((repetitions (alist-get 'repetitions task))) ;
                     (format " [%d/%d]"
                             (car repetitions)
                             (cdr repetitions))
                   "")
                 ;; name
                 (propertize
                  (alist-get 'name task)
                  'face (if (seq-contains-p task 'done)
                            'towel-my-done
                          'default))
                 ;; tags
                 (string-join
                  (mapcar
                   (lambda (tag)
                     (propertize (format "%S" tag)
                                 'face 'towel-tags))
                   (sort (funcall towel-get-tags-function task)
                         (lambda (a b)
                           (string> (format "%S" a) (format "%S" b)))))
                  " ")))))



(defun towel-my-set-done (task)
  "Add `done' tag to TASK."
  (towel-add-tag task 'done))

(defun towel-my-update-repetitions (task)
  "Update TASK `repetitions' tag."
  (let* ((repetitions (alist-get 'repetitions task))
         (current (1+ (car repetitions)))
         (total (cdr repetitions)))
    (if (>= current total)
        (if (alist-get 'sched task)
            (progn
              (setf (alist-get 'repetitions task)
                    (cons 0 total))
              (towel-my-update-sched task))
          (towel-my-set-done task))
      (setf (alist-get 'repetitions task)
            (cons current total))
      (towel--add task))))

(defun towel-my-update-sched (task)
  "Update TASK `sched' tag."
  (let* ((next (alist-get 'next-time task))
         (next-time
          (cond
           ((numberp next) (* 60 60 24 next)) ; Convert days to seconds
           ((functionp next)
            (funcall next (time-to-seconds) (alist-get 'sched task)))
           (t (* 60 60 24))))
         (new-sched (towel-my-round-seconds
                     (+ (- (time-to-seconds) towel-my-day-start)
                        next-time))))
    (setf (alist-get 'sched task) new-sched)
    (towel--add task)))

(defun towel-done-dwim ()
  "Deduce action which should be made when task is finished."
  (interactive)
  (let ((task (towel--task-under-point)))
    (cond
     ((alist-get 'repetitions task)
      (towel-my-update-repetitions task))
     ((alist-get 'sched task)
      (towel-my-update-sched task))
     (t
      (towel-my-set-done task))))
  (towel-revert-buffer))

(defun towel-open-tasks-file ()
  "Open `towel-file' via `find-file'."
  (interactive)
  (find-file towel-file))

(define-key towel-mode-map (kbd "d") #'towel-done-dwim)
(define-key towel-mode-map (kbd "o") #'towel-open-tasks-file)

(provide 'towel-my)
;;; towel-my.el ends here
